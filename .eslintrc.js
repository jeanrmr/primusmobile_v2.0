module.exports = {
  extends: 'airbnb',
  parser: 'babel-eslint',
  env: {
    jest: true
  },
  rules: {
    'no-use-before-define': 'off',
    'react/jsx-filename-extension': 'off',
    'react/prop-types': 'off',
    'comma-dangle': 'off',
    semi: 'off',
    'import/prefer-default-export': 'off',
    'react/jsx-indent': 'tab',
    'react/jsx-indent-props': 'tab'
  },
  globals: {
    fetch: false
  },
};
