/**
 * @format
 */

import React from 'react';
// import configureMockStore from 'redux-mock-store'
// Note: test renderer must be required after react-native.
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import AsyncStorage from '@react-native-community/async-storage';
import Intro from '../src/common/components/testTest/index';
import reducer, { Types, Creators, saveUser } from '../src/common/components/testTest/duck';
// import { LoginScreen } from '../src/screens/Login';
// import Loader from '../src/common/components/Loader';

// initialState = {
//   test: { count: 0 }
// };

// createStore = configureStore([thunk, promise]);

// describe('loader snap test', () => {
//   wrapper = shallow(<Loader store={createStore(initialState)} />)
//     .childAt(0)
//     .dive();

//   it('default store', () => {
//     expect(wrapper).toMatchSnapshot();
//   });

//   it('loading true', () => {
//     wrapper.setProps({ loading: false });
//     expect(wrapper).toMatchSnapshot();
//   });
// });

// describe('test actionCreators', () => {
// 	it('inc', () => {
// 		expectedAction = {

// 		}
// 	})
// })

// describe('test real connect', () => {
//   let store;
//   let wrapper;

//   rerender = () => {
//     wrapper = shallow(<Intro store={store} />);
//   };

//   beforeEach(() => {
//     initialState = {
//       test: { count: 0 }
//     };

//     createStore = configureStore([thunk, promise]);
//     store = createStore(initialState);
//     rerender();
//   });

//   it('renders correctly', () => {
//     console.log(wrapper.debug());
//     store.dispatch(Creators.incCount());
//     store.dispatch(Creators.incCount());
//     store.dispatch(Creators.incCount());
//     console.log(store.getState());
//     rerender();
//     console.log(wrapper.debug());

//   });
// });

describe('test AsyncStorage values', () => {
  let store;
	let wrapper;

  rerender = () => {
    wrapper = shallow(<Intro store={store} />);
  };

  beforeEach(() => {
    initialState = {
      test: { count: 0 },
      go: true
    };

    createStore = configureStore([thunk, promise]);
    store = createStore(initialState);
    rerender();
  });

  // it('renders correctly', () => {
  //   console.log(wrapper.debug());
  //   store.dispatch(Creators.saveUser());
  //   console.log(store.getState());
  //   rerender();
  //   console.log(wrapper.debug());
  // });

  it('dispatch correct values', async () => {
		let initialValue = 'Jean'
		action = saveUser(initialValue)
		await action(store.dispatch);
		let value = await AsyncStorage.getItem('user')
		console.log(value);

		expect(value).toBe(initialValue)
  });

  // it('save value on async',  (a) => {

  // });
});

// describe('test render', () => {
//   const onButtonClickMock = jest.fn();
//   wrapper = shallow(<Intro inc={onButtonClickMock} />);

//   it('renders correctly', () => {
//     expect(wrapper).toMatchSnapshot();
//   });

//   it('render the aeoh list', () => {
//     wrapper.setProps({ count: 5 });
//     expect(wrapper.find('Text')).toHaveLength(5);
//   });

//   it('buttonClick', () => {
//     wrapper
//       .find('button')
//       .simulate('click')
//       .simulate('click');
//     expect(onButtonClickMock).toHaveBeenCalledTimes(2);
//   });
// });
