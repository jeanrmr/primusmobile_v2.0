import { Generator } from 'battlecry';

const ROOT_REDUCER =  'rootReducer'

export default class ComponentGenerator extends Generator {

  config = {
    generate: {
      args: 'name ...actions ?',
      options: {
        special: { description: 'Special option' }
      }
    }
  };

  generate() {
    this.templates().forEach(file => file.saveAs(`src/${this.args.name}/`, this.args.name));
  }
}
