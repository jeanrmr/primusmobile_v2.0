import React from 'react';
import { bindActionCreators } from 'redux';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';

const __Name__ = props => <View />;

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

connect(
	mapStateToProps,
	mapDispatchToProps
)(__Name__);
