import React from 'react';
import { bindActionCreators } from 'redux';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';

class __Name__ extends React.Component {
	render() {
		return (
			<View>
			</View>
		);
	}
}

const style = StyleSheet.create({});

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

connect(
	mapStateToProps,
	mapDispatchToProps
)(__Name__);
