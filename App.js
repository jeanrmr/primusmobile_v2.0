/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
// import SplashScreen from 'react-native-splash-screen'
import { Provider } from 'react-redux';
import Navigation from './src/navigation';
import store from './src/store';
import httpConfig from './src/common/HttpConfig';
import NavigationService from './src/navigation/NavigationService';
import Loader from './src/common/components/Loader';

export default class App extends Component {
  constructor(props) {
    super(props);
    httpConfig();
  }

  componentWillMount() {
    console.disableYellowBox = true;
  }

  componentDidMount() {
    // SplashScreen.hide()
  }

  render() {
    return (
      <Provider store={store}>
        <Loader />
        <Navigation
          ref={(navigatorRef) => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}
