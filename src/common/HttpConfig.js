const sefazServer = 'http://10.7.156.59'
const tmpServer = 'http://10.6.194.102'
const localServer = 'http://192.168.15.11'
const localDevServer = 'http://192.168.0.10'
const appUrl = ':3003/primus/api/'

import Axios from 'axios'

export default () => {
	Axios.defaults.baseURL = `${localDevServer}${appUrl}`;
	Axios.defaults.timeout = 3000;
}