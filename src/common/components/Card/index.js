import React from 'react';
import {
  View, Text, TouchableOpacity, StyleSheet
} from 'react-native';
import * as colors from '~/common/colors';

const Card = (props) => {
  if (props.onPress) {
    return (
        <TouchableOpacity onPress={() => this.props.onPress()} style={{...style.card, ...props.style}} >
          {props.children}
        </TouchableOpacity>
      )
  }else {
    return (
      <View style={{...style.card, ...props.style}} >
        {props.children}
      </View>
    )
  }
};

export default Card;

const style = StyleSheet.create({
  card: {
    backgroundColor: colors.backgroundCard,
    height: 90,
    marginBottom: 10,
    padding: 10,
    borderRadius: 5,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 3,
    elevation: 2
  }
});
