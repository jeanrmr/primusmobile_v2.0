import React from 'react';
import { View } from 'react-native';

export default (Separator = props => (
  <View
    style={{
      flexDirection: 'row'
    }}
  >
    <View
      style={{
        borderBottomColor: '#555',
        borderBottomWidth: 1,
        borderColor: '#ddd',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        elevation: 1,
        flex: 2

      }}
    />
  </View>
));
