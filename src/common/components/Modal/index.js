import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import RnModal from 'react-native-modal';
import { Icon } from 'native-base';
import * as colors from '../../colors';

export default class Modal extends Component {
  static propTypes = {
    isVisible: PropTypes.bool,
    toogleModal: PropTypes.func
  };

  render() {
    const { isVisible, toggleModal } = this.props;
    return (
      <RnModal
        isVisible={isVisible}
        backdropColor="#fff"
        backdropOpacity={0.20}
        animationIn="slideInUp"
        animationOut="slideOutDown"
        transparent
      >
        <View style={{ flex: 1, alignItems: 'center' }}>
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.modalContainer}>
              <TouchableOpacity
                transparent
                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                onPress={() => toggleModal()}
                style={styles.closeIconContainer}
              >
                <Icon name="close" style={styles.closeIcon} />
              </TouchableOpacity>
              <View style={styles.contentContainer}>{this.props.children}</View>
            </View>
          </View>
        </View>
      </RnModal>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    borderRadius: 10,
    height: '95%',
    width: '95%',
    paddingRight: '3%',
    paddingLeft: '3%',
    paddingBottom: '3%',
    backgroundColor: colors.backgroundDefault,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 3,
    elevation: 2
  },
  closeIcon: {
    color: colors.defaultText,
    fontSize: 28,
    height: 27,
    padding: 0,
    margin: 0
  },
  closeIconContainer: {
    alignSelf: 'flex-end',
    position: 'absolute',
    width: 20,
    height: 20,
    alignItems: 'center',
    zIndex: 1
  },
  contentContainer: {
    paddingLeft: 13,
    paddingRight: 13,
    paddingBottom: 13,
    paddingTop: 22,
    height: '100%',
    width: '100%'
  }
});
