import React from 'react'
import { SafeAreaView } from 'react-navigation';
import { Text, TouchableOpacity, StyleSheet } from "react-native";
import * as colors  from "../../colors";


const BottomTabBar = props => {
	const {
		renderIcon,
		getLabelText,
		activeTintColor,
		inactiveTintColor,
		onTabPress,
		onTabLongPress,
		getAccessibilityLabel,
		navigation
	  } = props;

	  const { routes, index: activeRouteIndex } = navigation.state;

  return (
	<SafeAreaView style={{...styles.container, backgroundColor: colors.highBackground}}>
		 {routes.map((route, routeIndex) => {
        const isRouteActive = routeIndex === activeRouteIndex;
        const tintColor = isRouteActive ? activeTintColor : inactiveTintColor;

        return (
          <TouchableOpacity
            key={routeIndex}
            style={styles.tabButton}
            onPress={() => {
              onTabPress({ route });
            }}
            onLongPress={() => {
              onTabLongPress({ route });
            }}
            accessibilityLabel={getAccessibilityLabel({ route })}
          >
            {renderIcon({ route, focused: isRouteActive, tintColor })}

          </TouchableOpacity>
        );
      })}
	</SafeAreaView>
  )
}

const styles = StyleSheet.create({
	container: { flexDirection: "row", height: 52, elevation: 2 },
	tabButton: { flex: 1, justifyContent: "center", alignItems: "center" }
})

export default BottomTabBar
