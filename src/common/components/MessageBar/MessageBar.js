import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { removeMessage } from '../../../store/ducks/message'

class MessageBar extends Component {
	render() {
		const { message } = this.props
		const messageView = (
			<View style={styles.container}>
				<Text style={{ color: 'yellow' }}>
					{message || null}
				</Text>
			</View>
		);

		if (message) {
			setTimeout(() => {
				this.props.removeMessage()
			}, 3000);
			return messageView
		}

		return null;
	}
}

function mapStateToProps(state) {
	return {
		message: state.message.text
	};
}

const mapDispatchToProps = dispatch => bindActionCreators({ removeMessage }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(MessageBar);

const styles = StyleSheet.create({
	container: {
		alignItems: 'center',
		position: 'absolute',
		bottom: 0,
		left: 0,
		right: 0,
		backgroundColor: 'rgba(52,52,52, 1.0)',
		padding: 10,
	}
});
