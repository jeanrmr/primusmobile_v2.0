import React from 'react';
// import configureMockStore from 'redux-mock-store'
// Note: test renderer must be required after react-native.
import { shallow } from 'enzyme';
import {Loader} from "~/common/components/Loader";

describe('render tests', () => {
  wrapper = shallow(<Loader loading={true} />);
  it('match snap', () => {
    expect(wrapper).toMatchSnapshot();
  })

  it('visible false',() => {
    expect(wrapper.props()['visible']).toBe(true)
    wrapper.setProps({loading: false })
    expect(wrapper.props()['visible']).toBe(false)
  })
})
