import React from 'react';
import {
  StyleSheet, View, Modal, ActivityIndicator, Text
} from 'react-native';
import { connect } from 'react-redux';

export class Loader extends React.Component {
  render() {
    return (
      <Modal
        visible={this.props.loading}
        animationType="none"
        transparent
        onRequestClose={() => 'modal closed'}
      >
        <View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <ActivityIndicator animating={this.props.loading} size="large" color="#BD833F" />
          </View>
        </View>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.loader.loading,
});

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: 'rgba(255, 255, 255, 0.4)'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#202D39',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
});

export default connect(mapStateToProps)(Loader);
