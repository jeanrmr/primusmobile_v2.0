import React from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Image,
  PixelRatio,
  Alert,
  StyleSheet,
  Platform
} from 'react-native';
import { Icon } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import PropTypes from 'prop-types';
import * as colors from '../../colors';

const size = PixelRatio.getPixelSizeForLayoutSize;

export default class Header extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    marginBottom: PropTypes.number,
    back: PropTypes.bool,
    hideOut: PropTypes.bool
  };

  defaultMarginBottom = 220;

  blur = Platform.OS === 'ios' ? 5.0 : 2.0;

  getMarginBottom = () => {
    if (this.props.marginBottom != undefined) {
      return this.props.marginBottom;
    }
    if (this.props.children != undefined) {
      return 30;
    }
    return this.defaultMarginBottom;
  };

  linearBackground = () => {
    if (!this.props.children) {
      return (
        <View>
          <LinearGradient
            colors={['rgba(0,0,0, 1)', 'rgba(0,0,0, 0.0)']}
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              ...ifIphoneX({ height: 120 }, { height: 60 })
            }}
          />

          <LinearGradient
            colors={['rgba(0,0,0, 0.5)', 'rgba(0,0,0, 0.0)']}
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              ...ifIphoneX({ height: 120 }, { height: 60 })
            }}
          />
        </View>
      );
    }
    return (
      <LinearGradient
        colors={['rgba(0,0,0, 0.4)', 'rgba(0,0,0, 0.4)']}
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
          ...ifIphoneX({ height: 230 }, { height: 200 })
        }}
      />
    );
  };

  render() {
    return (
      <View style={{ marginBottom: this.getMarginBottom() }}>
        <View style={styles.backgroundImageContainer}>
          <Image
            blurRadius={this.props.children ? this.blur : 0}
            style={styles.backgroundImage}
            source={require('../../../../assets/homeHeader.jpg')}
          />
        </View>

        {this.linearBackground()}

        <LinearGradient
          colors={['rgba(32,45,57, 0)', 'rgba(32,45,57, 1)']}
          style={styles.linearGradient}
        />

        <View style={styles.contentContainer}>
          <Text style={styles.title}>{this.props.title}</Text>

          {this.props.back ? (
            <TouchableOpacity
              transparent
              style={{ alignSelf: 'flex-start', position: 'absolute' }}
              hitSlop={{
                top: 10,
                bottom: 10,
                left: 10,
                right: 10
              }}
              onPress={() => {
                this.props.onGoBack ? this.props.onGoBack() : '';
                this.props.navigation.goBack();
              }}
            >
              <Icon name="arrow-round-back" style={styles.backIcon} />
            </TouchableOpacity>
          ) : null}

          {this.props.hideOut ? null : (
            <TouchableOpacity transparent style={{ alignSelf: 'flex-end', position: 'absolute' }}>
              <Icon
                name="log-out"
                style={{ color: colors.defaultText, marginRight: size(3), marginTop: size(2) }}
                onPress={() => this.showConfirmDialog()}
                hitSlop={{
                  top: 10,
                  bottom: 10,
                  left: 10,
                  right: 10
                }}
              />
            </TouchableOpacity>
          )}

          {this.props.children}
        </View>
      </View>
    );
  }

  showConfirmDialog = () => {
    Alert.alert(
      '',
      'Deseja realmente sair?',
      [
        { text: 'Sair', onPress: () => this.props.navigation.navigate('auth') },
        { text: 'Cancel', style: 'cancel' }
      ],
      { cancelable: true }
    );
  };
}

const styles = StyleSheet.create({
  contentContainer: {
    ...ifIphoneX({ marginTop: 30 })
  },
  title: {
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 18,
    alignSelf: 'center',
    position: 'absolute',
    marginTop: size(5)
  },
  backgroundImageContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    overflow: 'hidden',
    ...ifIphoneX({ height: 230 }, { height: 200 })
  },
  backgroundImage: {
    ...ifIphoneX({ height: 230 }, { height: 200 }),
    width: '100%'
  },
  linearGradient: {
    position: 'absolute',
    ...ifIphoneX({ top: 190 }, { top: 160 }),
    left: 0,
    width: '100%',
    height: 40
  },
  backIcon: {
    fontSize: 38,
    color: colors.defaultText,
    marginLeft: size(3),
    marginTop: size(1)
  }
});
