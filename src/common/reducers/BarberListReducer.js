const INITAL_STATE =  { barbers: []}

export default (state = INITAL_STATE, action)  => {
    switch(action.type){
        case 'LOAD_BARBERS':
            return {...state, barbers: action.payload}

        default: {
            return state
        }
    }
}