import Axios from 'axios';
import { enableLoading, disableLoading } from '../store/ducks/loader';
import { showMessage } from '../store/ducks/message';

export const getBarbeiros = () => async dispatch => {
  try {
    dispatch(enableLoading());
    Axios.get('barber')
      .then((res) => {
        dispatch({ type: 'LOAD_BARBERS', payload: res.data });
        dispatch(disableLoading());
      })
      .catch((error) => {
        dispatch(disableLoading());
        dispatch(showMessage(error.message));
      });
  } catch (error) {
    dispatch(disableLoading());
    dispatch(showMessage(error.message));
  }
};
