import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { Container, Icon } from 'native-base';

const initialState = {}

export default class BarberEditScreen extends Component {
	static navigationOptions = ({ navigation }) => {
		const { firstName, lastName } = navigation.state.params
		return {
			title: (`${firstName}' '${lastName}`),
			headerLeft: (
				<TouchableOpacity transparent onPress={() => navigation.pop()} style={{ marginLeft: 10 }}>
					<Icon name="arrow-round-back" style={{ color: '#BD833F' }} />
				</TouchableOpacity>
			),
			headerStyle: {
				backgroundColor: '#10171D'
			},
			headerTitleStyle: {
				color: '#BD833F'
			}
		}
	};

	render() {
		return (
			<Container style={{ backgroundColor: '#202D39' }} />
		);
	}
}
