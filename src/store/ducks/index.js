import { reducer as formReducer } from 'redux-form';
import { combineReducers } from 'redux';
import AuthReducer from './auth';
import LoaderReducer from './loader';
import MessageReducer from './message';
import BarberListReducer from '../../common/reducers/BarberListReducer';

export const rootReducer = combineReducers({
  credentials: AuthReducer,
  loader: LoaderReducer,
  message: MessageReducer,
  barberList: BarberListReducer,
  form: formReducer
});
