import Axios from 'axios';
import { AsyncStorage } from 'react-native';
import { disableLoading, enableLoading } from '../loader';
import { saveUser } from '../auth';
import { showMessage } from '../message';

export const login = value => (dispatch) => {
  dispatch(enableLoading());
  Axios.post('auth/login', value)
    .then((res) => {
      dispatch(save(res.data));
    })
    .catch((err) => {
      dispatch(disableLoading());
      dispatch(showMessage({ text: err.message }));
    });
};

export const save = response => async (dispatch) => {
  try {
    await AsyncStorage.setItem('user', JSON.stringify(response));
    dispatch(saveUser(response));
    Axios.defaults.headers.Authorization = `bearer ${response.token}`;
    dispatch(disableLoading());
  } catch (error) {
    dispatch(disableLoading());
    dispatch(showMessage({ text: error.message }));
  }
};
