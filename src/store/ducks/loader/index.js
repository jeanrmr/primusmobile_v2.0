import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  enable: [],
  disable: []
});

const INITIAL_STATE = { loading: false };

const enable = (state = INITIAL_STATE) => ({
  ...state,
  loading: true
});

const disable = (state = INITIAL_STATE) => ({
  ...state,
  loading: false
});

export default createReducer(INITIAL_STATE, {
  [Types.ENABLE]: enable,
  [Types.DISABLE]: disable
});
