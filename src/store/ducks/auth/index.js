import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import { createActions, createReducer } from "reduxsauce";
import { Creators as loader } from '../loader';
import { showMessage } from '../message';

export const { Types, Creators } = createActions({
  saveUser: ['value'],
  removeUser: []
})

export function verifyUserInfo(navigation) {
  return async (dispatch) => {
    try {
      let user = await AsyncStorage.getItem('user');
      if(user) {
        user = JSON.parse(user);
        Axios.defaults.headers.Authorization = `bearer ${user.token}`;
        dispatch(Creators.saveUser((user)));
      }
      navigation.navigate(user ? 'app' : 'auth');
      dispatch(loader.disable());
    } catch (err) {
      dispatch(showMessage(err));
    }
  };
}

export function removeUser() {
  return async (dispatch) => {
    dispatch(loader.enable());
    try {
      await AsyncStorage.removeItem('user')
      Axios.defaults.headers.Authorization = undefined;
      dispatch(Creators.removeUser());
      dispatch(loader.disable());
    } catch (error) {
      dispatch(loader.disable());
      dispatch(showMessage(error));
    }
  };
}

const INITIAL_STATE = { verified: false, user: null };

export default createReducer(INITIAL_STATE, {
  [Types.SAVE_USER] : saveReducer,
  [Types.REMOVE_USER] : removeReducer
})

const saveReducer = (state = INITIAL_STATE, value) => ({
  ...state, user: value, verified: true
})

const removeReducer = (state = INITIAL_STATE) => ({
  ...state, user: undefined, verified: false
})
