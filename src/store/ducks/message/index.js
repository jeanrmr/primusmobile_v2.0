import { createActions, createReducer } from "reduxsauce";

export const {Types, Creators} = createActions({
  showMessage: ['message'],
  removeMessage: []
})

const INITIAL_STATE = { type: null, message: null };

const showMessage = (state = INITIAL_STATE, message) => ({
  ...state,
        type: message.type,
        text: message.text
})
const removeMessage = (state = INITIAL_STATE) => ({
  ...state,
        type: null,
        text: null
})

export default createReducer(INITIAL_STATE, {
  [Types.SHOW_MESSAGE]: showMessage,
  [Types.SHOW_MESSAGE]: removeMessage
})

