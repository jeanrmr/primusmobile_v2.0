import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { createStore, applyMiddleware, compose } from 'redux';
import { rootReducer } from './ducks';
import promise from 'redux-promise'
import Reactotron from '../../ReactotronConfig'

const middlewares = [thunk, promise];

const composer = __DEV__
  ? compose(
    applyMiddleware(...middlewares),
    Reactotron.createEnhancer(),
  )
  : compose(applyMiddleware(...middlewares));

export default createStore(rootReducer, composer);
