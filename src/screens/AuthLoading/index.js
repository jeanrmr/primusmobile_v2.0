import React, { Component } from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { verifyUserInfo } from '~/store/ducks/auth';

class AuthLoadingScreen extends Component {
  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    const { verifyUserInfo, navigation } = this.props;
    verifyUserInfo(navigation);
  }

  render() {
    return (
      <View style={style.parent}>
        <StatusBar hidden />
        <View style={style.container} />
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  parent: {
    flex: 1,
    backgroundColor: '#202D39'
  }
});

const mapDispatchToProps = dispatch => bindActionCreators({ verifyUserInfo }, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(AuthLoadingScreen);
