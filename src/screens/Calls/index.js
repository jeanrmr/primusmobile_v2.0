import React, { Component } from 'react';
import {
  StatusBar, StyleSheet, ScrollView, Text, View, FlatList
} from 'react-native';
import { Container } from 'native-base';
import Header from '../../common/components/Header';
import * as colors from '../../common/colors';
import Card from '~/common/components/Card';

export default class Calls extends Component {
  calls = [
    {
      data: '10/01/19',
      profissional: 'nome',
      status: 'realizado'
    },
    {
      data: '10/02/19',
      profissional: 'nome2',
      status: 'realizado'
    },
    {
      data: '10/07/19',
      profissional: 'nome3',
      status: 'agendado'
    },
    {
      data: '30/06/19',
      profissional: 'nome3',
      status: 'cancelado'
    }
  ];

  render() {
    return (
      <Container style={style.container}>
        <StatusBar hidden />
        <ScrollView>
          <Header marginBottom={200} navigation={this.props.navigation} title="Atendimentos" />
          <View style={{padding: 10}}>
            <FlatList
              data={this.calls}
              renderItem={({ item, index }) => this.renderCalls(item, index)}
            />
          </View>
        </ScrollView>
      </Container>
    );
  }

  renderCalls = (item, index) => (
    <Card>
      <Text style={{color: colors.secondaryText}}>{item.data}</Text>
      <Text style={{color: colors.secondaryText}}>{item.profissional}</Text>
      <Text style={{color: colors.secondaryText}}>{item.status}</Text>
    </Card>
  );
}

const style = StyleSheet.create({
  container: {
    backgroundColor: colors.backgroundDefault
  }
});
