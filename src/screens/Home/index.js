import React, { Component } from 'react';
import { StatusBar, StyleSheet, ScrollView } from 'react-native';
import { Container } from 'native-base';
import Header from '../../common/components/Header'
import Calls from './Calls'
import * as colors from '../../common/colors'

export default class HomeScreen extends Component {
	render() {
		return (
			<Container style={style.container}>
				<StatusBar hidden />
				<ScrollView style={{ width: '100%', height: '100%' }}>
					<Header marginBottom={220} navigation={this.props.navigation} title="Barbearia Primus"/>
					<Calls />
				</ScrollView>
			</Container>
		)
	}
}

const style = StyleSheet.create({
	container: {
		backgroundColor: colors.backgroundDefault,
		alignItems: 'center'
	}
})
