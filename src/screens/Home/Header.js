import React from 'react';
import {
  View, TouchableOpacity, Text, Image, PixelRatio, Alert, StyleSheet
} from 'react-native';
import { Icon } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { ifIphoneX } from 'react-native-iphone-x-helper';

const size = PixelRatio.getPixelSizeForLayoutSize;

export default class Header extends React.Component {
  showConfirmDialog = () => {
    Alert.alert(
      '',
      'Deseja realmente sair?',
      [
        { text: 'Sair', onPress: () => this.props.navigation.navigate('auth') },
        { text: 'Cancel', style: 'cancel' }
      ],
      { cancelable: true }
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: 200,
            borderBottomLeftRadius: 100,
            overflow: 'hidden'
          }}
        >
          <Image
            style={{ height: 200, width: '100%' }}
            source={require('../../../assets/homeHeader.jpg')}
          />
        </View>

        <LinearGradient
          colors={['rgba(0,0,0, 0.8)', 'rgba(0,0,0, 0.2)']}
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: 200,
            borderBottomLeftRadius: 100
          }}
        />

        <View style={styles.contentContainer}>
          <Text style={styles.title}>HOME</Text>

          <TouchableOpacity transparent style={{ alignSelf: 'flex-end', position: 'absolute' }}>
            <Icon
              name="log-out"
              style={{ color: '#FFF', marginRight: size(2), marginTop: size(2) }}
              onPress={() => this.showConfirmDialog()}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...ifIphoneX({ marginBottom: 180 }, { marginBottom: 220 })
  },
  contentContainer: {
    ...ifIphoneX({ marginTop: 30 })
  },
  title: {
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 18,
    alignSelf: 'center',
    position: 'absolute',
    marginTop: size(5)
  }
});
