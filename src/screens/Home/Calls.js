import React, { Component } from 'react';
import {
  StyleSheet, View, Text, FlatList, Image, PixelRatio
} from 'react-native';

const size = PixelRatio.getPixelSizeForLayoutSize;

export default class Calls extends Component {
  calls = [
    require('../../../assets/calls/call1.jpg'),
    require('../../../assets/calls/call2.jpg'),
    require('../../../assets/calls/call3.jpg'),
    require('../../../assets/calls/call4.jpg')
  ];

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Atendimentos</Text>
        <FlatList
          data={this.calls}
          horizontal
          renderItem={({ item }) => this.renderCallCard(item)}
        />
      </View>
    );
  }

  renderCallCard(item) {
    return (
      <View style={styles.card}>
        <Image
          style={{
            height: 400,
            width: 250,
            borderRadius: 20
          }}
          source={item}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 20
  },
  title: {
    fontSize: 24,
    color: '#fff',
    marginBottom: 10,
    paddingLeft: size(10)
  },
  card: {
    marginRight: 20
  }
});
