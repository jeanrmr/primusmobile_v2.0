import React, { Component } from 'react';
import {
  Text, Alert, TouchableOpacity, StyleSheet, View, FlatList, Image
} from 'react-native';
import { Icon } from 'native-base';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { _getBarbeiros } from '../../barber/BarberListActions';
import * as colors from '~/common/colors';
import { NavigationActions } from 'react-navigation';
import Separator from '~/common/components/Separator';
import NavigationService from '~/navigation/NavigationService';

class ProfessionalList extends Component {
  barbers = [
    {
      firstName: 'Jean',
      lastName: 'Monteiro',
      id: 1,
      firstName: 'Jean',
      lastName: 'Monteiro',
      facebook: 'asds',
      instagram: 'sldkas',
      bio: 'asdas',
      photo: null,
      services: [
        {
          name: 'Barba'
        },
        {
          name: 'Cabelo'
        }
      ]
    },
    {
      id: 1,
      firstName: 'Jean',
      lastName: 'Monteiro',
      facebook: 'asds',
      instagram: 'sldkas',
      bio: 'asdas',
      photo: null,
      services: [
        {
          name: 'Barba'
        },
        {
          name: 'Cabelo'
        }
      ]
    },
    {
      id: 1,
      firstName: 'Jean',
      lastName: 'Monteiro',
      facebook: 'asds',
      instagram: 'sldkas',
      bio: 'asdas',
      photo: null,
      services: [
        {
          name: 'Barba'
        },
        {
          name: 'Cabelo'
        }
      ]
    }
  ];

  static navigationOptions = () => ({
    title: 'Profissionais',
    headerLeft: (
      <TouchableOpacity
        transparent
        onPress={() => NavigationService.goBack()}
        style={{ marginLeft: 10 }}
      >
        <Icon name="arrow-round-back" style={{ color: colors.hightLightText }} />
      </TouchableOpacity>
    ),
    headerRight: (
      <TouchableOpacity
        transparent
        onPress={() => NavigationService.navigate('editProfessionalInfo')}
        style={{ marginRight: 10 }}
      >
        <Icon name="add" style={{ color: colors.hightLightText }} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: '#10171D'
    },
    headerTitleStyle: {
      color: '#BD833F'
    }
  });

  render() {
    return (
      <View style={{ backgroundColor: colors.backgroundDefault, flex: 1 }}>
        <FlatList
          style={{ paddingLeft: 15, paddingRight: 15 }}
          data={this.barbers}
          renderItem={({ item, index }) => this.renderBarberCard(item, index)}
        />
      </View>
    );
  }

  componentWillMount() {
    this.props.navigation.addListener('willFocus', () => {
     NavigationService.hideBottomBar('profile')
    });
  }

  renderBarberCard(barber, index) {
    finalBarberCardStyle = { ...style.barberCard, flexDirection: 'row' };
    if (index == 0) finalBarberCardStyle = { ...finalBarberCardStyle, marginTop: 10 };
    return (
      <TouchableOpacity onPress={() => {}} key={barber._id} style={finalBarberCardStyle}>
        {this.renderBarberPhoto(barber)}

        <View
          style={{
            marginLeft: 10,
            flexDirection: 'column',
            flex: 1,
            justifyContent: 'space-between'
          }}
        >
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ color: colors.hightLightText }}>
              {`${barber.firstName} ${barber.lastName}`}
            </Text>
          </View>

          <Separator />

          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <View style={{ flexDirection: 'column', marginTop: 5 }}>
              {this.renderBarberServices(barber)}
            </View>

            <View style={{ flexDirection: 'column', justifyContent: 'flex-end' }}>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <TouchableOpacity onPress={() => Alert.alert('Em construção :)')}>
                  <Icon type="FontAwesome" name="instagram" style={style.socialIcons} />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => Alert.alert('Em construção :)')}>
                  <Icon type="FontAwesome" name="facebook" style={style.socialIcons} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  renderBarberServices(barber) {
    return barber.services.map(service => (
      <Text key={service._id} style={{ marginRight: 5, color: colors.secondaryText }}>
        {service.name}
      </Text>
    ));
  }

  renderBarberPhoto(barber) {
    if (barber.photoB64) {
      return (
        <View
          style={{
            padding: 10,
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 10,
            borderRadius: 5,
            width: 70,
            height: 70
          }}
        >
          <Image
            style={{ width: 70, height: 70, borderRadius: 5 }}
            source={{ uri: barber.photoB64 }}
          />
        </View>
      );
    }
    return (
      <View style={style.photoContainer}>
        <Icon name="person" style={{ color: 'rgba(52, 65, 77, 1)', fontSize: 70 }} />
      </View>
    );
  }
}

const mapStateToProps = state => ({ barbers: state.barberList.barbers });

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    _getBarbeiros
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfessionalList);

const style = StyleSheet.create({
  barberCard: {
    backgroundColor: 'rgba(52, 65, 77, 1)',
    height: 90,
    marginBottom: 10,
    padding: 10,
    borderRadius: 5,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 3,
    elevation: 2
  },
  photoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.highBackground,
    borderRadius: 5,
    marginRight: 10,
    width: 70,
    height: 70
  },
  socialIcons: {
    color: colors.hightLightText,
    marginLeft: 10,
    fontSize: 22
  }
});
