import React, { Component } from 'react';
import { Text, View, PixelRatio } from 'react-native';
import { FlatList, TouchableOpacity } from 'react-native';
import { CheckBox } from 'native-base';
import * as colors from '../../common/colors';

const size = PixelRatio.getPixelSizeForLayoutSize;

export default class ServiceList extends Component {
  render() {
    return (
      <FlatList
        data={this.props.serviceList}
        extraData={this.state}
        renderItem={({ item, index }) => this.renderService(item, index)}
      />
    );
  }

  renderService = (item, index) => (
    <TouchableOpacity
      style={{ flexDirection: 'row', height: 50 }}
      onPress={() => {
        this.props.serviceList[index].checked
          ? (this.props.serviceList[index].checked = false)
          : (this.props.serviceList[index].checked = true);
        this.setState({ ...this.state, serviceList: this.props.serviceList });
      }}
    >
      <View
        style={{
          alignItems: 'center',
          flexDirection: 'row',
          flex: 1,
          borderColor: 'black',
          borderBottomWidth: 1
        }}
      >
        <View style={{ alignItems: 'center', flexDirection: 'row', marginRight: 25 }}>
          <CheckBox checked={item.checked} color={colors.hightLightText} />
        </View>

        <View>
          <Text style={{ color: colors.defaultText }}>{item.name}</Text>
        </View>
      </View>

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          borderColor: 'black',
          borderBottomWidth: 1,
          paddingRight: size(2)
        }}
      >
        <View style={{ flexDirection: 'column', alignItems: 'center' }}>
          <Text style={{ color: colors.defaultText }}>{`R$${item.price},00`}</Text>
          <Text style={{ color: colors.secondaryText }}>{`${item.time}min`}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}
