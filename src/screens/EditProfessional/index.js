import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  ScrollView,
  StatusBar,
  PixelRatio
} from 'react-native';
import {
  Form, Item, Label, Input, Container
} from 'native-base';
import { RkButton } from 'react-native-ui-kitten';
import Header from '~/common/components/Header';
import * as colors from '~/common/colors';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import HeaderContent from './HeaderContent';

const size = PixelRatio.getPixelSizeForLayoutSize;

export class EditProfessional extends Component {
  static navigationOptions = () => ({
    header: null,
    headerStyle: {
      backgroundColor: colors.highBackground
    }
  });

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar hidden />
        <ScrollView>
          <Header
            marginBottom={15}
            title="Editar Profissional"
            back
            hideOut
            onGoBack={this.showBottomBar}
            navigation={this.props.navigation}
          >
            <HeaderContent />
          </Header>

          <KeyboardAvoidingView style={styles.formContainer} behavior="padding" enabled>
            <Form>
              <Item floatingLabel style={{ borderBottomColor: colors.hightLightText }}>
                <Label style={{ color: colors.secondaryText }}>Nome</Label>
                <Input style={{ color: colors.hightLightText }} returnKeyType="next" />
              </Item>

              <Item floatingLabel style={{ borderBottomColor: colors.hightLightText }}>
                <Label style={{ color: colors.secondaryText }}>Sobrenome</Label>
                <Input style={{ color: colors.hightLightText }} />
              </Item>

              <Item floatingLabel style={{ borderBottomColor: colors.hightLightText }}>
                <Label style={{ color: colors.secondaryText }}>Username</Label>
                <Input style={{ color: colors.hightLightText }} />
              </Item>

              <Item floatingLabel style={{ borderBottomColor: colors.hightLightText }}>
                <Label style={{ color: colors.secondaryText }}>Username</Label>
                <Input style={{ color: colors.hightLightText }} />
              </Item>

              <Item floatingLabel style={{ borderBottomColor: colors.hightLightText }}>
                <Label style={{ color: colors.secondaryText }}>Username</Label>
                <Input style={{ color: colors.hightLightText }} />
              </Item>

              <Item floatingLabel style={{ borderBottomColor: colors.hightLightText }}>
                <Label style={{ color: colors.secondaryText }}>Username</Label>
                <Input style={{ color: colors.hightLightText }} />
              </Item>

              <Item floatingLabel style={{ borderBottomColor: colors.hightLightText }}>
                <Label style={{ color: colors.secondaryText }}>Username</Label>
                <Input style={{ color: colors.hightLightText }} />
              </Item>
            </Form>

            <View style={styles.navButtonsContainer}>
              <View style={{ alignItems: 'center', flex: 1 }}>
                <RkButton
                  style={{
                    backgroundColor: colors.backgroundDefault,
                    borderRadius: 10,
                    width: '50%',
                    borderColor: colors.hightLightText
                  }}
                  contentStyle={{ color: colors.hightLightText }}
                  onPress={() => {}}
                >
                  Salvar
                </RkButton>
              </View>
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </Container>
    );
  }
}

export const mapStateToProps = state => ({});

export const mapDispatchToProps = {};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.backgroundDefault
  },
  formContainer: {
    flex: 1,
    marginLeft: '5%',
    marginRight: '5%',
    marginBottom: '5%',
    paddingRight: '5%',
    paddingBottom: '5%',
    backgroundColor: 'rgba(52, 65, 77, 1)',
    borderRadius: 5,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 3,
    elevation: 2
  },
  item: {
    color: '#ddd',
    marginBottom: 15
  },
  itemTextContainer: {
    justifyContent: 'center',
    marginLeft: 10
  },
  navButtonsContainer: {
    marginTop: 30,
    flexDirection: 'row',
    ...ifIphoneX({ alignItems: 'flex-end' }, { alignItems: 'center' }),
    ...ifIphoneX({ height: size(20) }, { height: size(30) })
  }
});
