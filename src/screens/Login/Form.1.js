import React from 'react';
import {
  StyleSheet, TextInput, PixelRatio, KeyboardAvoidingView
} from 'react-native';
import { View } from 'native-base';
import { RkButton } from 'react-native-ui-kitten';
import { withFormik } from 'formik';
import NavigationService from '../../navigation/NavigationService';

const size = PixelRatio.getPixelSizeForLayoutSize;

const LoginForm = props => (
    <KeyboardAvoidingView style={style.formContainer} behavior="padding" enabled>
      <View style={{ flexDirection: 'row' }}>{renderPhone(props)}</View>

      <View style={{ flexDirection: 'row' }}>{renderPassword(props)}</View>

      <View style={{ flexDirection: 'row' }}>
        <RkButton
          style={style.button}
          contentStyle={{ color: '#BD833F' }}
          onPress={props.handleSubmit}
        >
          ENTRAR
        </RkButton>
      </View>
    </KeyboardAvoidingView>
);

const renderPhone = props => (
  <TextInput
    placeholderTextColor="#BD833F"
    placeholder="Telefone com DDD"
    onChangeText={text => props.setField('phone', text)}
    value={props.values.phone}
    style={{ ...style.textInput, marginBottom: size(10) }}
  />
);

const renderPassword = props => (
  <TextInput
    placeholderTextColor="#BD833F"
    value={this.props.values.password}
    style={{ ...style.textInput, marginBottom: size(10) }}
    placeholder="Senha"
    textContentType="password"
    secureTextEntry
    underlineColorAndroid="transparent"
    onChangeText={text => props.setField('password', text)}
  />
);

const style = StyleSheet.create({
  formContainer: {
    flexDirection: 'column',
    alignItems: 'center'
  },
  textInput: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    borderRadius: 20,
    color: '#BD833F',
    height: 40,
    flex: 0.8
  },
  button: {
    flex: 0.8,
    borderRadius: 20,
    backgroundColor: '#202D39'
  }
});

export default withFormik({
  mapPropsToValues: () => ({ phone: '', password: '' }),

  handleSubmit: (credentials) => {
    NavigationService.navigate('app');
  }
})(LoginForm);
