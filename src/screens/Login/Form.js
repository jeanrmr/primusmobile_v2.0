import React from 'react';
import {
  StyleSheet, TextInput, PixelRatio, KeyboardAvoidingView
} from 'react-native';
import { View } from 'native-base';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { RkButton } from 'react-native-ui-kitten';
import { login } from '../../store/ducks/login/login';
import NavigationService from '../../navigation/NavigationService';

const size = PixelRatio.getPixelSizeForLayoutSize;

const LoginForm = (props) => {
  const { handleSubmit } = props;

  const submitForm = (credentials) => {
    this.props.login(credentials);
  };

  return (
    <KeyboardAvoidingView style={style.formContainer} behavior="padding" enabled>
      <View style={{ flexDirection: 'row' }}>
        <Field name="phone" component={renderPhone} />
      </View>

      <View style={{ flexDirection: 'row' }}>
        <Field name="password" component={renderPassword} />
      </View>

      <View style={{ flexDirection: 'row' }}>
        <RkButton
          style={style.button}
          contentStyle={{ color: '#BD833F' }}
          // onPress={handleSubmit(submitForm)}
          onPress={() => {
            NavigationService.navigate('app');
          }}
        >
          ENTRAR
        </RkButton>
      </View>
    </KeyboardAvoidingView>
  );
};

const renderPhone = ({ input: { onChange } }) => (
  <TextInput
    placeholderTextColor="#BD833F"
    placeholder="Telefone com DDD"
    onChangeText={onChange}
    style={{ ...style.textInput, marginBottom: size(10) }}
  />
);

const renderPassword = ({ input: { onChange } }) => (
  <TextInput
    placeholderTextColor="#BD833F"
    style={{ ...style.textInput, marginBottom: size(10) }}
    placeholder="Senha"
    textContentType="password"
    secureTextEntry
    underlineColorAndroid="transparent"
    onChangeText={onChange}
  />
);

const style = StyleSheet.create({
  formContainer: {
    flexDirection: 'column',
    alignItems: 'center'
  },
  textInput: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    borderRadius: 20,
    color: '#BD833F',
    height: 40,
    flex: 0.8
  },
  button: {
    flex: 0.8,
    borderRadius: 20,
    backgroundColor: '#202D39'
  }
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    login
  },
  dispatch
);

const connectedForm = connect(
  null,
  mapDispatchToProps
)(LoginForm);

export default reduxForm({ form: 'loginForm' })(connectedForm);
