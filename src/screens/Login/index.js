import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ImageBackground,
  PixelRatio,
  TouchableOpacity,
  Text,
  SafeAreaView,
  Platform
} from 'react-native';

import { RkButton } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators } from '../../store/ducks/loader';
import LoginForm from './Form';
// import LoginForm from './Form';

const size = PixelRatio.getPixelSizeForLayoutSize;
const blur = Platform.OS === 'ios' ? 7.0 : 2.4;

export class LoginScreen extends Component {
  static navigationOptions = {
    header: null
  };

  componentDidUpdate() {
    // const { userToken, navigation } = this.props
    // if (userToken) {
    // 	navigation.navigate('app')
    // }
  }

  render() {
    return (
      <View style={style.parent}>
        <StatusBar hidden />
        <ImageBackground
          blurRadius={blur}
          source={require('../../../assets/login_background_cut.jpg')}
          style={style.backgroundImage}
        >
          <View style={style.loginContainer}>
            <SafeAreaView>
              <View style={style.loginForm}>
                <Image
                  source={require('../../../assets/logo2.png')}
                  style={style.logo}
                  resizeMode="contain"
                />

                <LoginForm navigation={this.props.navigation} login={() => {}}/>

                <View>
                  <TouchableOpacity
                    style={{
                      marginTop: 20,
                      alignItems: 'center'
                    }}
                  >
                    <Text>Esqueceu sua senha?</Text>
                    <Text style={{ fontWeight: 'bold' }}>Solicite uma nova</Text>
                  </TouchableOpacity>
                </View>

                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                  <RkButton
                    style={{
                      flex: 0.8,
                      backgroundColor: '#4266B2',
                      borderRadius: 20
                    }}
                    contentStyle={{ color: '#FFF' }}
                    onPress={() => {
                      this.props.enable();
                    }}
                  >
                    Entrar com Facebook
                  </RkButton>
                </View>

                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    marginTop: 10,
                    justifyContent: 'center',
                    marginBottom: 20
                  }}
                >
                  {/* <Text>Ainda não tem cadastro?</Text> */}
                  <Text>{this.props.teste}</Text>
                  <Text style={{ fontWeight: 'bold' }}> Cadastrar-se!</Text>
                </TouchableOpacity>
              </View>
            </SafeAreaView>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToPros = state => ({
  userToken: state.credentials.user ? state.credentials.user.token : undefined
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    enable: Creators.enableLoader
  },
  dispatch
);

export default connect(
  mapStateToPros,
  mapDispatchToProps
)(LoginScreen);

const style = StyleSheet.create({
  parent: {
    flex: 1
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  backgroundImage: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(255, 255, 255, 1)'
  },
  loginContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginForm: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    flex: 1
  }
});
