import React, { Component } from 'react';
import {
  StatusBar, StyleSheet, ScrollView, View, TouchableOpacity, Text
} from 'react-native';
import { Container, Icon } from 'native-base';
import Header from '../../common/components/Header';
import * as colors from '../../common/colors';
import HeaderContent from './HeaderContent';
import NavigationService from '~/navigation/NavigationService';
import reactotron from 'reactotron-react-native';

export default class Profile extends Component {
  componentWillMount() {
    this.props.navigation.addListener('willFocus', () => {
      NavigationService.showBottomBar('profile');
    });
  }

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar hidden />
        <ScrollView contentContainerStyle={{ width: '100%', height: '100%' }}>
          <Header title="Perfíl" navigation={this.props.navigation}>
            <HeaderContent user="Jean Monteiro" role="Administrador" />
          </Header>
          {this.renderOptions()}
        </ScrollView>
      </Container>
    );
  }

  renderOptions = () => (
    <View style={styles.optionsContainer}>
      <TouchableOpacity
        style={{ ...styles.itemContainer, borderTopWidth: 0.5 }}
        onPress={() => this.props.navigation.navigate('editProfile')}
      >
        <View style={{ flexDirection: 'row', flex: 1, padding: 10 }}>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <Icon name="create" style={styles.iconItem} />
          </View>

          <View style={styles.itemTextContainer}>
            <Text style={styles.itemLabel}>Alterar minha informações</Text>
          </View>

          <View style={{ flex: 1, alignItems: 'flex-end' }}>
            <Icon name="arrow-forward" style={styles.iconItem} />
          </View>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={styles.itemContainer}>
        <View style={{ flexDirection: 'row', flex: 1, padding: 10 }}>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <Icon name="calendar" style={styles.iconItem} />
          </View>
          <View style={styles.itemTextContainer}>
            <Text style={styles.itemLabel}>Atendimentos</Text>
          </View>

          <View style={{ flex: 1, alignItems: 'flex-end' }}>
            <Icon name="arrow-forward" style={styles.iconItem} />
          </View>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={styles.itemContainer} onPress={() => {}}>
        <View style={{ flexDirection: 'row', flex: 1, padding: 10 }}>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <Icon name="alert" style={styles.iconItem} />
          </View>
          <View style={styles.itemTextContainer}>
            <Text style={styles.itemLabel}>Solicitações</Text>
          </View>

          <View style={{ flex: 1, alignItems: 'flex-end' }}>
            <Icon name="arrow-forward" style={styles.iconItem} />
          </View>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={styles.itemContainer} onPress={() => {}}>
        <View style={{ flexDirection: 'row', flex: 1, padding: 10 }}>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <Icon name="cut" style={styles.iconItem} />
          </View>
          <View style={styles.itemTextContainer}>
            <Text style={styles.itemLabel}>Serviços</Text>
          </View>

          <View style={{ flex: 1, alignItems: 'flex-end' }}>
            <Icon name="arrow-forward" style={styles.iconItem} />
          </View>
        </View>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => this.props.navigation.navigate('professionals')}
      >
        <View style={{ flexDirection: 'row', flex: 1, padding: 10 }}>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <Icon name="person" style={styles.iconItem} />
          </View>
          <View style={styles.itemTextContainer}>
            <Text style={styles.itemLabel}>Profissionais</Text>
          </View>
          <View style={{ flex: 1, alignItems: 'flex-end' }}>
            <Icon name="arrow-forward" style={styles.iconItem} />
          </View>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={styles.itemContainer} onPress={() => {}}>
        <View style={{ flexDirection: 'row', flex: 1, padding: 10 }}>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <Icon name="build" style={styles.iconItem} />
          </View>
          <View style={styles.itemTextContainer}>
            <Text style={styles.itemLabel}>Configurações</Text>
          </View>
          <View style={{ flex: 1, alignItems: 'flex-end' }}>
            <Icon name="arrow-forward" style={styles.iconItem} />
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );

  changeProfile = () => {};
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.backgroundDefault
  },
  itemContainer: {
    borderBottomWidth: 0.5,
    flexDirection: 'row',
    height: 50,
    borderColor: colors.hightLightText
  },
  iconItem: {
    color: '#ddd',
    alignItems: 'center'
  },
  optionsContainer: {
    flex: 1,
    marginTop: 10
  },
  itemLabel: {
    color: '#ddd'
  },
  itemTextContainer: {
    justifyContent: 'center',
    marginLeft: 10
  }
});
