import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Text
} from 'react-native';

export default class SignupScreen extends Component {
  render() {
    return (
      <View style={style.parent}>
        <View style={style.imageContainer}>
          <Text>Signup little brother!</Text>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  Image: {
    width: 200,
    height: 200
  },
  parent: {
    flex: 1
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
