import React, { Component } from 'react'
import { StyleSheet, View, Image, Text, FlatList, TouchableOpacity } from 'react-native'
import moment from "moment";
import * as colors from "../../common/colors";
import { ifIphoneX } from "react-native-iphone-x-helper";

export default class TimeBarbers extends Component {

	/*MOCK DATA */
	getData() {
		data = [
			{
				photo: require('../../../assets/mock_barbers_pics/marcelo.png'),
				times: []
			},
			{
				photo: require('../../../assets/mock_barbers_pics/marcelo.png'),
				times: []
			},
			{
				photo: require('../../../assets/mock_barbers_pics/marcelo.png'),
				times: []
			},
			{
				photo: require('../../../assets/mock_barbers_pics/marcelo.png'),
				times: []
			},
			{
				photo: require('../../../assets/mock_barbers_pics/marcelo.png'),
				times: []
			}
		]
		data.forEach(e => {
			m = moment().millisecond(0).second(0).minute(0).hour(8);
			for (let index = 0; index < 8; index++) {
				m.add(10, 'm')
				e.times.push(m.format('HH:mm'))
			}
		})
		return data;
	}
	/*MOCK DATA */

	constructor(props) {
		super(props)
	}

	renderTimeItem = time => (
		<View style={styles.timeContainer}>
			<TouchableOpacity onPress={() => this.props.toggleModal()} >
				<Text style={styles.time}>
					{time}
				</Text>
			</TouchableOpacity>
		</View>
	)

	renderTimeLineList = timeLine => (

		<View style={styles.borderBottom}>
			<View style={styles.container}>
				<View style={styles.photoContainer}>
					<Image
						style={styles.photo}
						source={timeLine.photo}
					/>
				</View>

				<FlatList
					data={timeLine.times}
					horizontal={true}
					renderItem={({ item }) => this.renderTimeItem(item)}
				/>
			</View>
		</View>
	)

	render() {
		return (
			<FlatList style={{ ...ifIphoneX({ marginBottom: 30 }, { marginBottom: 10 }) }}
				data={this.getData()}
				renderItem={({ item }) => this.renderTimeLineList(item)}
			/>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1, flexDirection: 'row',
		marginTop: 10,
		marginLeft: 10,
		marginBottom: 10
	},
	photoContainer: {
		marginRight: 10
	},
	photo: {
		width: 60, height: 60, borderRadius: 30
	},
	timeContainer: {
		alignItems: 'center',
		marginRight: 10,
		flexDirection: 'row',
	},
	time: {
		borderColor: 'white',
		borderWidth: 1,
		padding: 8,
		borderRadius: 5,
		color: colors.defaultText
	},
	borderBottom: {
		borderColor: 'white',
		borderBottomWidth: 0.5,
	}
})
