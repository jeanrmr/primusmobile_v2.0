import React, { Component } from 'react';
import { StatusBar, StyleSheet } from 'react-native';
import { Container } from 'native-base';
import Header from './Header'
import * as colors from '../../common/colors'
import ScheduleTopNavigation from "../../navigation/ScheduleTopNavigation";

export default class SetSchedule extends Component {
	static router = ScheduleTopNavigation.router;
	render() {

		return (
			<Container style={style.container}>
				<StatusBar hidden />
				<Header navigation={this.props.navigation} />
				<ScheduleTopNavigation navigation={this.props.navigation} />
			</Container>
		)
	}
}

const style = StyleSheet.create({
	container: {
		backgroundColor: colors.backgroundDefault
	}
})