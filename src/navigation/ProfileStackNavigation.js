import { createStackNavigator} from 'react-navigation'
import Profile from '../screens/Profile';
import EditProfile from "~/screens/EditProfile";
import ProfessionalsStack from "~/navigation/ProfessionalsStack";

export default createStackNavigator({
	profile: {
		screen: Profile,
		navigationOptions: props => ({
			header: null
		})
  },
  editProfile: {
    screen: EditProfile,
    navigationOptions: props => ({
			header: null
		})
  },
  professionals: {
    screen: ProfessionalsStack,
    navigationOptions: props => ({
			header: null
		})
  }

})
