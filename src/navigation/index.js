import React from 'react';
import { createAppContainer } from 'react-navigation';
import LoginStackNavigation from '~/navigation/LoginStackNavigation';
import AppBottomNavigation from '~/navigation/AppBottomNavigation';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';

import AuthLoadingScreen from '~/screens/AuthLoading';
import { Transition } from 'react-native-reanimated';

const switchNavigation = createAnimatedSwitchNavigator(
  {
    loading: AuthLoadingScreen,
    app: AppBottomNavigation,
    auth: LoginStackNavigation
  },
  {
    transition: (
      <Transition.Together>
        <Transition.In type="fade" durationMs={500} interpolation="easeInOut"/>
        <Transition.Out type="slide-bottom" durationMs={500} interpolation="easeInOut" />
      </Transition.Together>
    ),
    initialRouteName: 'loading'
  }
);

export default createAppContainer(switchNavigation);
