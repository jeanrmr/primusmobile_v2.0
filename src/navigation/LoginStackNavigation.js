import { createStackNavigator } from 'react-navigation';
import LoginScreen from '~/screens/Login';
import SignUpScreen from '~/screens/Signup';

const authStackNav = createStackNavigator(
  {
    signIn: LoginScreen,
    signUp: SignUpScreen
  },
  {
    gesturesEnabled: true,
    initialRouteName: 'signIn',
    mode: 'modal',
    headerTransitionPreset: 'fade-in-place'
  }
);
export default authStackNav;
