import React from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation';
import { Icon } from 'native-base';
import HomeScreen from '../screens/Home';
import ProfileStackNavigation from './ProfileStackNavigation';
import * as colors from '../common/colors';
import SetScheduleScreen from '../screens/SetSchedule';
import CallsScreen from '../screens/Calls';
import BottomTabBar from '../common/components/BottomTabBar';

export default createMaterialTopTabNavigator(
  {
    home: {
      screen: HomeScreen,
      navigationOptions: ({ navigation }) => ({
        header: null,
        tabBarIcon: ({ focused, tintColor }) => (
          <Icon name="home" style={{ color: tintColor, fontSize: 27 }} />
        ),
        tabBarVisible: true
      })
    },
    setSchedule: {
      screen: SetScheduleScreen,
      navigationOptions: ({ navigation }) => ({
        header: null,
        tabBarIcon: ({ focused, tintColor }) => (
          <Icon name="cut" style={{ color: tintColor, fontSize: 27 }} />
        ),
        tabBarVisible: false
      })
    },
    calls: {
      screen: CallsScreen,
      navigationOptions: ({ navigation }) => ({
        header: null,
        tabBarIcon: ({ focused, tintColor }) => (
          <Icon name="calendar" style={{ color: tintColor, fontSize: 27 }} />
        ),
        tabBarVisible: tavBarVisible(navigation.state.params)
      })
    },
    profile: {
      screen: ProfileStackNavigation,
      navigationOptions: ({ navigation }) => ({
        header: null,
        tabBarIcon: ({ focused, tintColor }) => (
          <Icon name="contact" style={{ color: tintColor, fontSize: 27 }} />
        ),
        tabBarVisible: tavBarVisible(navigation.state.params)
      })
    }
  },
  {
    initialRouteName: 'home',
    tabBarPosition: 'bottom',
    animationEnabled: true,
    backBehavior: 'history',
    swipeEnabled: false,
    tabBarComponent: props => <BottomTabBar {...props} />,
    tabBarOptions: {
      style: {
        backgroundColor: colors.highBackground
      },
      activeTintColor: colors.hightLightText,
      inactiveTintColor: colors.defaultText,
      showIcon: true,
      showLabel: false,
      renderIndicator: () => null
    }
  }
);

tavBarVisible = (params) => {
  if (params && params.tabBarVisible != undefined) return params.tabBarVisible;
  return true;
};
